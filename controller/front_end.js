//INDEX PAGE
exports.index_page=function(req,res){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.render("index",{page_name:"index",title:"Eatoo",message:req.query.message});
}

//SEARCH PAGE
exports.search_page=function(req,res){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.render("search",{page_name:"search",title:"Eatoo",message:req.query.message});
}
var front_end=require("./front_end.js");

exports.routes=function(app) {

    //INDEX PAGE
    app.get("/",front_end.index_page);

    //SEARCH PAGE
    app.get("/search",front_end.search_page);

};
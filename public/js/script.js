function initAutocomplete(a) {
	var t = document.getElementById("googleMap"),
		n = {
			center: new google.maps.LatLng(28, 77),
			zoom: 10
		},
		o = (new google.maps.Map(t, n), document.getElementById("location_store")),
		e = new google.maps.places.SearchBox(o);
	e.addListener("places_changed", function() {
		var a = e.getPlaces();
		if (0 != a.length) {
			new google.maps.LatLngBounds;
			a.forEach(function(a) {
				if (!a.geometry) return void console.log("Returned place contains no geometry");
				var t = a.geometry.location.lat(),
					n = a.geometry.location.lng();
				$("input[name='location_name']").val(a.name), $("input[name='location_lat']").val(t), $("input[name='location_long']").val(n), $("#submit_search").html('<img src="/public/images/rolling.svg"/>'), processForm(t, n, a.name)
			})
		}
	})
}

function setSort(a) {
	$("input[name='type_sort']").val(a), 1 == a ? $(".type").html('<span class="glyphicon glyphicon-filter"></span> Sort by Price <span class="glyphicon glyphicon-menu-down"></span>') : 2 == a ? $(".type").html('<span class="glyphicon glyphicon-filter"></span> Sort by Rating <span class="glyphicon glyphicon-menu-down"></span>') : 3 == a && $(".type").html('<span class="glyphicon glyphicon-filter"></span> Sort by Distance <span class="glyphicon glyphicon-menu-down"></span>')
}

function processForm(a, t, n) {
	$(".head-info").html("Showing Restaurants in <span>" + n + "</span>");
	var o, e = $("input[name='type_sort']").val();
	1 == e ? o = "https://developers.zomato.com/api/v2.1/search?lat=" + a + "&lon=" + t + "&radius=20000&sort=cost" : 2 == e ? o = "https://developers.zomato.com/api/v2.1/search?lat=" + a + "&lon=" + t + "&radius=20000&sort=rating" : 3 == e && (o = "https://developers.zomato.com/api/v2.1/search?lat=" + a + "&lon=" + t + "&radius=20000&sort=real_distance"), $.ajax({
		url: o,
		type: "get",
		headers: {
			"user-key": "c213528b3fb32851d604b2c3619b7d94",
			Accept: "application/json"
		},
		dataType: "json",
		success: function(a) {
			updateHTML(a)
		}
	})
}

function updateHTML(a) {
	for (var t = "", n = 0; n <= a.restaurants.length - 1; n++) t += '<div class="thumbnail type-card" id="' + a.restaurants[n].restaurant.R.res_id + '"> <div class="row"> <div class="col-xs-3"> <div class="food-picture" style="background-image:url(' + a.restaurants[n].restaurant.thumb + ');";></div> </div> <div class="col-xs-9 top"> <a href="#" class="rating-btn"> <i class="fa fa-star"></i> ' + a.restaurants[n].restaurant.user_rating.aggregate_rating + ' </a> <p class="lighter">DETAILS</p> <h3>' + a.restaurants[n].restaurant.name + '</h3> <p class="bold">' + a.restaurants[n].restaurant.location.locality + '</p> <p class="light">' + a.restaurants[n].restaurant.location.address + '</p> </div> </div> <div class="separator"></div> <div class="row bottom"> <div class="col-xs-3"> <p class="light">CUISINES</p> <p class="light">COST FOR TWO</p> <p class="light">DISCOUNTS</p> </div> <div class="col-xs-9"> <p class="normal">' + a.restaurants[n].restaurant.cuisines + '</p> <p class="normal"><i class="fa fa-inr"></i> ' + a.restaurants[n].restaurant.average_cost_for_two + '</p> <p class="normal">' + manupulateDiscounts(a.restaurants[n].restaurant.offers) + '</p> </div> </div> <div class="right some-padding"> <a href="#" class="map_type" onclick="zoomMap(' + a.restaurants[n].restaurant.location.latitude + "," + a.restaurants[n].restaurant.location.longitude + ');"> <i class="fa fa-map-marker"></i> LOCATION </a> <a href="#" class="call_type" onclick="openPhoneModal(' + a.restaurants[n].restaurant.phone_numbers + ",`" + a.restaurants[n].restaurant.name + '`);"> <i class="fa fa-phone"></i> CALL </a> <a href="#" class="book_type" onclick="openTableBooking(' + a.restaurants[n].restaurant.has_table_booking + ",`" + a.restaurants[n].restaurant.url + '`);"> <i class="fa fa-shopping-cart"></i> BOOK A TABLE </a> </div> </div>';
	$(".result-box").html(t), loadGoogleMaps(a), $("#submit_search").html("Search")
}

function manupulateDiscounts(a) {
	return 0 == a.length ? "NO OFFERS" : void 0
}

function openPhoneModal(a, t) {
	$("#phoneModal .modal-title").html(t), void 0 == a ? $("#phoneModal .phone-number").html("NOT AVIALABLE") : $("#phoneModal .phone-number").html(a[0]), $("#phoneModal").modal("show")
}

function openTableBooking(a, t) {
	1 == a ? window.open(t) : ($(".notification-bar").css("visibility", "visible"), $(".notification-bar").slideDown())
}

function loadGoogleMaps(a) {
	var t = document.getElementById("googleMap"),
		n = {
			center: new google.maps.LatLng($("input[name='location_lat']").val(), $("input[name='location_long']").val()),
			zoom: 12
		};
	map = new google.maps.Map(t, n);
	for (var o = a.restaurants.length - 1; o >= 0; o--) {
		var e = new google.maps.Marker({
				position: new google.maps.LatLng(a.restaurants[o].restaurant.location.latitude, a.restaurants[o].restaurant.location.longitude),
				icon: "/public/images/marker.png"
			}),
			s = new google.maps.InfoWindow({
				content: ""
			});
		bindInfoWindow(e, map, s, '<p class="font">' + a.restaurants[o].restaurant.name + "</p>", a.restaurants[o].restaurant.R.res_id), e.setMap(map)
	}
}

function bindInfoWindow(a, t, n, o, e) {
	google.maps.event.addListener(a, "click", function() {
		n.setContent(o), n.open(t, a), window.setTimeout(function() {
			t.setZoom(20), t.setCenter(a.getPosition()), $(".result-box").animate({
				scrollTop: 0
			}, 0, function() {
				$(".result-box").animate({
					scrollTop: $("#" + e).offset().top - 150
				}, 1e3)
			})
		}, 500)
	})
}

function zoomMap(a, t) {
	map.setCenter(new google.maps.LatLng(a, t)), map.setZoom(15)
}
$(document).ready(function() {
	$(".search-pg").slideUp()
}), $(window).load(function() {
	$(".notification-bar").slideUp(), $("#submit_search").click(function() {
		0 != $("input[name='location_name']").val().trim().length && 0 != $("input[name='location_lat']").val().trim().length && 0 != $("input[name='location_long']").val().trim().length && ($("#submit_search").html('<img src="/public/images/rolling.svg"/>'), $("input[name='location']").val($("input[name='location_name']").val()), processForm($("input[name='location_lat']").val(), $("input[name='location_long']").val(), $("input[name='location_name']").val()))
	})
});
var flag = 0;
$("input[name='location']").focus(function() {
	0 == flag && ($(".search-pg").slideDown(), $(".search-pg").css("visibility", "visible"), $("html, body").animate({
		scrollTop: $(".search-pg").offset().top - $(".search-pg").height()
	}, 1e3, function() {
		$(".index-pg").slideUp(0)
	}), setTimeout(function() {
		flag = 1, $("#location_store").focus()
	}, 1e3))
}), $(".notification-bar .fa-close").click(function() {
	$(".notification-bar").slideUp()
});
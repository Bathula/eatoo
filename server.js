var express=require("express");
var hbs=require("hbs");

//CONTROLLERS OF VARIOUS USAGES
var routes=require("./controller/routes.js");
var handlebars=require("./controller/handlebars.js");

//INITIALIZE THE SOCEKT AND EXPRESS APP
app=express();
app.set("views",__dirname+"/view");
app.use('/public', express.static(__dirname + '/public'));
app.set('view engine','html');
app.engine('html',hbs.__express);

//INITIALIZE THE HANDLEBARS
handlebars.handlebars();
//SET THE ROUTES
routes.routes(app);

app.listen(process.env.PORT || 3000 ,function(req,res){
	console.log("-- EATOO APPLICATION STARTED --");
});
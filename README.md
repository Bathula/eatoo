<h2><b>Eatoo</b></h2>
<p><i style="font-size:18px;"><b>Eatoo</b></i> is a Node JS + Express + HBS implementation of Housing.com + Zomato styled restaurant discovery application that lets you search local restaurants based on locality.<br/><br/>
<a href="https://eatoo.herokuapp.com/" target="_blank">See Demo</a>
</p>
<hr/>
<h3><b>Steps</b></h3>
<ul>
	<li>
        <p>npm install [ to install all dependencies ]</p>
    </li>
    <li>
        <p>npm start [ to start server ]</p>
    </li>
</ul>
<hr/>
<p>Developed by Sreekar Bathula</a></p>